/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Logica.Calculo;
import java.awt.Color;
import javax.swing.JOptionPane;

/**
 *
 * @author AndresFWilT
 */
public class ventana extends javax.swing.JFrame {

    String oracion = "";
    private int va[];

    public ventana() {
        this.setTitle("Calculadora logica");
        this.setLocationRelativeTo(this);
        initComponents();
        va = new int[3];
        va[0] = 0;
        va[1] = 0;
        va[2] = 0;
        tabla.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToggleButton1 = new javax.swing.JToggleButton();
        jButton1 = new javax.swing.JButton();
        CajaTxt = new javax.swing.JTextField();
        bP = new javax.swing.JButton();
        bQ = new javax.swing.JButton();
        bR = new javax.swing.JButton();
        bY = new javax.swing.JButton();
        bO = new javax.swing.JButton();
        bNeg = new javax.swing.JButton();
        bEnt = new javax.swing.JButton();
        bsisolosi = new javax.swing.JButton();
        bCorizq = new javax.swing.JButton();
        bCorder = new javax.swing.JButton();
        bCalcular = new javax.swing.JButton();
        bDel = new javax.swing.JButton();
        bAC = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        jToggleButton1.setText("jToggleButton1");

        jButton1.setText("jButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        CajaTxt.setEnabled(true);
        CajaTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CajaTxtActionPerformed(evt);
            }
        });
        CajaTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                CajaTxtKeyTyped(evt);
            }
        });

        bP.setText("p");
        bP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bPActionPerformed(evt);
            }
        });

        bQ.setText("q");
        bQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bQActionPerformed(evt);
            }
        });

        bR.setText("r");
        bR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bRActionPerformed(evt);
            }
        });

        bY.setText("^");
        bY.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bYActionPerformed(evt);
            }
        });

        bO.setText("v");
        bO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bOActionPerformed(evt);
            }
        });

        bNeg.setText("¬");
        bNeg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bNegActionPerformed(evt);
            }
        });

        bEnt.setText("→");
        bEnt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEntActionPerformed(evt);
            }
        });

        bsisolosi.setText("↔");
        bsisolosi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bsisolosiActionPerformed(evt);
            }
        });

        bCorizq.setText("(");
        bCorizq.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCorizqActionPerformed(evt);
            }
        });

        bCorder.setText(")");
        bCorder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCorderActionPerformed(evt);
            }
        });

        bCalcular.setText("Calcular");
        bCalcular.setBackground(Color.green);
        bCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCalcularActionPerformed(evt);
            }
        });

        bDel.setText("Del");
        bDel.setBackground(Color.red);
        bDel.setForeground(Color.white);
        bDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDelActionPerformed(evt);
            }
        });

        bAC.setText("AC");
        bAC.setBackground(Color.red);
        bAC.setForeground(Color.white);
        bAC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bACActionPerformed(evt);
            }
        });

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tabla);

        jLabel1.setText("*No olvide usar corchetes*");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(CajaTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(bP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bY, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bsisolosi, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(bO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(bQ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(bCorizq, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(bR, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(bNeg, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(bCorder, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(bAC, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(bCalcular)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(bDel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(bEnt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CajaTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bAC)
                        .addGap(9, 9, 9)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bP)
                            .addComponent(bQ)
                            .addComponent(bR)
                            .addComponent(bDel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bY)
                            .addComponent(bO)
                            .addComponent(bNeg)
                            .addComponent(bEnt))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bsisolosi)
                            .addComponent(bCorizq)
                            .addComponent(bCorder)
                            .addComponent(bCalcular))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bEntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEntActionPerformed
        oracion = oracion + "=>";
        CajaTxt.setText(oracion);
        CajaTxt.requestFocus();
    }//GEN-LAST:event_bEntActionPerformed

    private void CajaTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CajaTxtActionPerformed

    }//GEN-LAST:event_CajaTxtActionPerformed

    private void CajaTxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CajaTxtKeyTyped
        char validar = evt.getKeyChar();
        if (Character.isLetter(validar) || Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(getContentPane(), "Ingrese valores unicamente por lo botones", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_CajaTxtKeyTyped

    private void bPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bPActionPerformed
        if (va[0] == 0) {
            va[0] = 1;
        }
        oracion = oracion + "p";
        CajaTxt.setText(oracion);
        CajaTxt.requestFocus();
    }//GEN-LAST:event_bPActionPerformed

    private void bQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bQActionPerformed
        if (va[1] == 0) {
            va[1] = 1;
        }
        oracion = oracion + "q";
        CajaTxt.setText(oracion);
        CajaTxt.requestFocus();
    }//GEN-LAST:event_bQActionPerformed

    private void bRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bRActionPerformed
        if (va[2] == 0) {
            va[2] = 1;
        }
        oracion = oracion + "r";
        CajaTxt.setText(oracion);
        CajaTxt.requestFocus();
    }//GEN-LAST:event_bRActionPerformed

    private void bYActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bYActionPerformed
        oracion = oracion + "^";
        CajaTxt.setText(oracion);
        CajaTxt.requestFocus();
    }//GEN-LAST:event_bYActionPerformed

    private void bOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bOActionPerformed
        oracion = oracion + "v";
        CajaTxt.setText(oracion);
        CajaTxt.requestFocus();
    }//GEN-LAST:event_bOActionPerformed

    private void bNegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bNegActionPerformed
        oracion = oracion + "-";
        CajaTxt.setText(oracion);
        CajaTxt.requestFocus();
    }//GEN-LAST:event_bNegActionPerformed

    private void bsisolosiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bsisolosiActionPerformed
        oracion = oracion + "<=>";
        CajaTxt.setText(oracion);
        CajaTxt.requestFocus();
    }//GEN-LAST:event_bsisolosiActionPerformed

    private void bCorizqActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCorizqActionPerformed
        oracion = oracion + "(";
        CajaTxt.setText(oracion);
        CajaTxt.requestFocus();
    }//GEN-LAST:event_bCorizqActionPerformed

    private void bCorderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCorderActionPerformed
        oracion = oracion + ")";
        CajaTxt.setText(oracion);
        CajaTxt.requestFocus();
    }//GEN-LAST:event_bCorderActionPerformed

    private void bDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDelActionPerformed
        if (oracion != null && oracion != "") {
            if (oracion.charAt(oracion.length() - 1) == 'p') {
                va[0] = 0;
            }
            if (oracion.charAt(oracion.length() - 1) == 'q') {
                va[1] = 0;
            }
            if (oracion.charAt(oracion.length() - 1) == 'r') {
                va[2] = 0;
            }
            oracion = oracion.substring(0, oracion.length() - 1);
            CajaTxt.setText(oracion);
            CajaTxt.requestFocus();
        } else {
            getToolkit().beep();
            JOptionPane.showMessageDialog(getContentPane(), "No hay nada que borrar", "Error", JOptionPane.ERROR_MESSAGE);
        };
    }//GEN-LAST:event_bDelActionPerformed

    private void bACActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bACActionPerformed
        va[0] = 0;
        va[1] = 0;
        va[2] = 0;
        oracion = "";
        CajaTxt.setText(oracion);
        CajaTxt.requestFocus();
        tabla.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{}
        ));
    }//GEN-LAST:event_bACActionPerformed

    private void bCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCalcularActionPerformed
        int var = va[0] + va[1] + va[2];
        modeloTabla(var);
        Calculo c = new Calculo(oracion, var);
        c.ordenar(oracion, var, tabla);
        CajaTxt.requestFocus();
    }//GEN-LAST:event_bCalcularActionPerformed

    private void modeloTabla(int var) {
        if (var == 2) {
            tabla.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "", "", ""
                    }
            ));
        } else if (var == 3) {
            tabla.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "", "", "", ""
                    }
            ));
        } else {
            tabla.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                    }
            ));
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField CajaTxt;
    private javax.swing.JButton bAC;
    private javax.swing.JButton bCalcular;
    private javax.swing.JButton bCorder;
    private javax.swing.JButton bCorizq;
    private javax.swing.JButton bDel;
    private javax.swing.JButton bEnt;
    private javax.swing.JButton bNeg;
    private javax.swing.JButton bO;
    private javax.swing.JButton bP;
    private javax.swing.JButton bQ;
    private javax.swing.JButton bR;
    private javax.swing.JButton bY;
    private javax.swing.JButton bsisolosi;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JTable tabla;
    // End of variables declaration//GEN-END:variables
}
