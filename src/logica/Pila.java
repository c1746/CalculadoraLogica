/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

/**
 *
 * @author AndresFWilT
 */
public class Pila {

    private Nodo inicio;
    private Nodo fin;

    public Pila() {
    }

    public void cab(String nombre) {
        inicio = new Nodo(nombre);
        inicio.setSig(inicio);
        fin = inicio;
    }

    public boolean siguiente() {
        if (inicio.getSig() == inicio) {
            return false;
        } else {
            return true;
        }

    }

    public int tamaño() {
        int c = 0;
        Nodo aux = inicio.getSig();
        while (aux != inicio) {
            c++;
            aux = aux.getSig();
        }
        return c;
    }

    public Nodo cabeza() {
        return inicio;
    }

    public void ingresar(String nombre) {

        Nodo aux = inicio.getSig();
        inicio.setSig(new Nodo(nombre
        ));
        inicio.getSig().setSig(aux);

    }

    public Nodo retirar() {

        Nodo aux = inicio.getSig();
        inicio.setSig(aux.getSig());
        if (aux.getSig() == inicio) {
            fin = inicio;
        }

        return aux;

    }
}
