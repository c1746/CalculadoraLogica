/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author AndresFWilT
 */
public class Calculo {

    private int entradas;
    private String oracion;
    private boolean casos[][];

    public Calculo(String O, int ent) {
        this.entradas = ent;
        this.oracion = O;
        casos();
    }

    public void ordenar(String entrada, int entradas, JTable vista) {

        Pila aux = new Pila();
        Pila res = new Pila();
        aux.cab("com");
        res.cab("com");

        Nodo a;

        int control = 0;

        for (int i = 1; i <= entrada.length(); i++) {

            if (entrada.charAt(entrada.length() - i) == ')') {
                aux.ingresar(entrada.charAt(entrada.length() - i) + "");

            }
            if (entrada.charAt(entrada.length() - i) == 'v') {
                aux.ingresar(entrada.charAt(entrada.length() - i) + "");
            }
            if (entrada.charAt(entrada.length() - i) == '^') {
                aux.ingresar(entrada.charAt(entrada.length() - i) + "");
            }
            if (entrada.charAt(entrada.length() - i) == '>') {
                if (entrada.charAt(entrada.length() - i - 2) == '<') {
                    aux.ingresar(entrada.charAt(entrada.length() - i - 2) + ""
                            + entrada.charAt(entrada.length() - i - 1) + ""
                            + entrada.charAt(entrada.length() - i));
                    control = 2;

                } else {
                    aux.ingresar(entrada.charAt(entrada.length() - i - 1) + ""
                            + entrada.charAt(entrada.length() - i));
                    control = 1;
                }

            }
            if (entrada.charAt(entrada.length() - i) == 'p'
                    || entrada.charAt(entrada.length() - i) == 'q'
                    || entrada.charAt(entrada.length() - i) == 'r') {

                if (entrada.charAt(entrada.length() - i - 1) == '-') {
                    res.ingresar(entrada.charAt(entrada.length() - i - 1) + ""
                            + entrada.charAt(entrada.length() - i));
                    control = 1;
                } else {
                    res.ingresar(entrada.charAt(entrada.length() - i) + "");
                }

            }

            if (entrada.charAt(entrada.length() - i) == '(') {

                a = aux.retirar();
                while (a.getNombre().equals(")") == false) {
                    res.ingresar(a.getNombre());
                    a = aux.retirar();
                }

            }

            i = control + i;
            control = 0;
        }

        while (aux.siguiente()) {
            res.ingresar(aux.retirar().getNombre());
        }

        String[] op = new String[res.tamaño()];
        int i = 0;
        while (res.siguiente()) {
            op[i] = res.retirar().getNombre();
            i++;
        }
        operaciones(op, entradas, vista);

    }

    public void operaciones(String[] op, int a, JTable vista) {

        boolean resultado = true;

        DefaultTableModel modelo;
        modelo = (DefaultTableModel) vista.getModel();
        int repeticiones = 0;
        if (a == 2) {
            repeticiones = 4;
            modelo.addRow(new Object[]{"p", "q", "resultado"});
        }
        if (a == 3) {
            repeticiones = 8;
            modelo.addRow(new Object[]{"p", "q", "r", "resultado"});
        }
        boolean aux = false;
        int max = op.length - 2;
        int ruta = 0;

        String base[] = new String[op.length];

        for (int i = 0; i < repeticiones; i++) {

            ruta = 0;

            for (int j = 0; j < base.length; j++) {
                base[j] = op[j] + "";
            }
            base = cambio(base, i);

            
            //siclo para resolver el problema
            do {

                //primer valor encontrado

                if ((base[ruta].equals("1") || base[ruta].equals("0")) && ruta != 0) {

                    int par = ruta;
                    par++;

                    while (base[par].equals("-1")) {
                        par++;
                    }

                    if (base[par].equals("1") || base[par].equals("0")) {

                        int primero = Integer.parseInt(base[ruta]);
                        int segundo = Integer.parseInt(base[par]);

                        if (base[ruta - 1].equals("v")) { //condicional o

                            if (primero == 0 && segundo == 0) {
                                base[ruta - 1] = "0";
                            } else {
                                base[ruta - 1] = "1";
                            }
                            base[ruta] = "-1";
                            base[par] = "-1";
                        }
                        if (base[ruta - 1].equals("^")) { //condicional y
                            if (primero == 1 && segundo == 1) {
                                base[ruta - 1] = "1";
                            } else {
                                base[ruta - 1] = "0";
                            }
                            base[ruta] = "-1";
                            base[par] = "-1";
                        }
                        if (base[ruta - 1].equals("=>")) { //condicional si p entonces q

                            if (primero == 1 && segundo == 0) {
                                base[ruta - 1] = "0";
                            } else {
                                base[ruta - 1] = "1";
                            }
                            base[ruta] = "-1";
                            base[par] = "-1";
                        }
                        if (base[ruta - 1].equals("<=>")) { //condicional y si solo si
                            if (primero == segundo) {
                                base[ruta - 1] = "1";
                            } else {
                                base[ruta - 1] = "0";
                            }
                            base[ruta] = "-1";
                            base[par] = "-1";
                        }

                        ruta = -1;
                    }

                }

                ruta++;
            } while (base[0].equals("1") == false && base[0].equals("0") == false);

            if (base[0].equals("1")) {
                resultado = true;
            } else {
                resultado = false;
            }

            if (repeticiones == 8) {
                modelo.addRow(new Object[]{casos[0][i], casos[1][i], casos[2][i], resultado});
            }
            if (repeticiones == 4) {
                modelo.addRow(new Object[]{casos[0][i], casos[1][i], resultado});
            }
        }

    }

    public String[] cambio(String[] base, int i) {

        for (int j = 0; j < base.length; j++) {
            if (base[j].equals("p")) {
                if (casos[0][i] == true) {
                    base[j] = "1";
                } else {
                    base[j] = "0";
                }
            }

            if (base[j].equals("q")) {
                if (casos[1][i] == true) {
                    base[j] = "1";
                } else {
                    base[j] = "0";
                }
            }

            if (base[j].equals("r")) {
                if (casos[2][i] == true) {
                    base[j] = "1";
                } else {
                    base[j] = "0";
                }
            }

            if (base[j].equals("-p")) {
                if (casos[0][i] == true) {
                    base[j] = "0";
                } else {
                    base[j] = "1";
                }
            }
            if (base[j].equals("-q")) {
                if (casos[1][i] == true) {
                    base[j] = "0";
                } else {
                    base[j] = "1";
                }
            }
            if (base[j].equals("-r")) {
                if (casos[2][i] == true) {
                    base[j] = "0";
                } else {
                    base[j] = "1";
                }
            }

        }

        return base;
    }

    public void casos() {

        casos = new boolean[3][8];
        casos[0][0] = true;
        casos[1][0] = true;
        casos[2][0] = true;

        casos[0][1] = false;
        casos[1][1] = true;
        casos[2][1] = true;

        casos[0][2] = true;
        casos[1][2] = false;
        casos[2][2] = true;

        casos[0][3] = false;
        casos[1][3] = false;
        casos[2][3] = true;

        casos[0][4] = true;
        casos[1][4] = true;
        casos[2][4] = false;

        casos[0][5] = false;
        casos[1][5] = true;
        casos[2][5] = false;

        casos[0][6] = true;
        casos[1][6] = false;
        casos[2][6] = false;

        casos[0][7] = false;
        casos[1][7] = false;
        casos[2][7] = false;

    }
}
